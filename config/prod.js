module.exports = {
    googleClientID: process.env.GOOGLE_CLIENT_ID,
    googleClientSecret: process.env.GOOGLE_CLIENT_SECRET,
    mongoURI: process.env.MONGODB_URI,
    cookieKey: process.env.COOKIE_KEY,
    paystackPublicKey: process.env.PAYSTACK_PUBLIC_KEY,
    paystackSecretKey: process.env.PAYSTACK_SECRET_KEY,
    sendgridKey: process.env.SENDGRID_KEY,
    domainUrl: process.env.DOMAIN_URL
}