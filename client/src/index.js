import "materialize-css/dist/css/materialize.min.css";
import React from "react";
import ReactDom  from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";

import App from "./component/App";
import Reducers from "./reducers";

const store = createStore(Reducers, {}, applyMiddleware(reduxThunk));

ReactDom.render(
    <Provider store={store}><App /></Provider>, 
    document.getElementById("root")
);
