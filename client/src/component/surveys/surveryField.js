import React from "react";

export default ({ input, meta: { error, touched }, label, placeholder })=> {
    return (
        <div>
            <label>{label}</label>
            <input placeholder={placeholder} {...input}  style={{marginBottom: "5px"}} />
            <span className="red-text" style={{marginBottom: "20px"}}>
                {touched && error}
            </span>
        </div>
    );
}