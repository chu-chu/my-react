// Show forms to user
import React, {Component} from "react";
import { reduxForm, Field } from "redux-form";
import _ from "lodash";
import surveyField from "./surveryField";
import { Link } from "react-router-dom";
import validateEmails from "../../utils/validateEmails";

import FIELDS from "./formField";

class SurveyForm extends Component {
    renderFields() {
        return _.map(FIELDS, ({ name, label, placeholder }) => {
            return (
                <Field placeholder={placeholder} label={label} type="text" name={name} key={name} component={surveyField} />
            );
        });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.props.handleSubmit(this.props.onSurveySubmit)}>
                    { this.renderFields() }
                    <Link to="/surveys" className="red left white-text waves-effect waves-light btn">
                    Cancel
                    <i className="material-icons right">close</i>
                    </Link>
                    <button type="submit" className="tral right white-text waves-effect waves-light btn">
                    Next
                    <i className="material-icons right">arrow_forward</i>
                    </button>
                </form>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};
    
    _.each(FIELDS, ({ name, error }) => {
        if(!values[name]) {
            errors[name] = error;
        }
    })

    errors.recipients = validateEmails(values.recipients || "");

    return errors;
}

export default reduxForm({
    validate,
    form: "surveyForm",
    destroyOnUnmount: false
})(SurveyForm);