// Show survey new
import React, {Component} from "react";
import SurveyForm from "./surveyForm";
import SurveyReview from "./surveyFormReview";
import { reduxForm } from "redux-form";

class SurveyNew extends Component {
    // This is the same thing with
//    constructor(props) {
//        super(props);

//        this.state = { new: true }
//    }

    // This due to bable extension
   state = { showFormReview: false };

   renderContent() {
       if (this.state.showFormReview) {
           return <SurveyReview 
            onCancel= { ()=> this.setState({ showFormReview: false }) }
           />;
       }

       return <SurveyForm onSurveySubmit={() => {
           this.setState({ showFormReview: true }) }} 
       />;
   }

    render() {
        return (
            <div>
                { this.renderContent() }
            </div>
        );
    }
}

export default reduxForm({
    form: "surveyForm"
})(SurveyNew);