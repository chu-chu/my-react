import React, { Component } from "react";
import moment from "moment"
import { connect } from "react-redux";
import { fetchSurveys } from "./../../actions";


class SurveyList extends Component {
    componentDidMount() {
        this.props.fetchSurveys();
    }

    renderSurveys() {
        try {
            return this.props.surveys.reverse().map( survey => {
                return (
                    <div className="card blue-grey darken-1" key={ survey._id }>
                        <div className="card-content white-text">
                            <span className="card-title">{ survey.title }</span>
                            <p>
                                { survey.body }
                            </p>
                            <p className="right">
                                Sent: { moment(survey.dateSent).fromNow() }
                            </p>
                        </div>
                        <div class="card-action">
                            <a>Yes: { survey.yes }</a>
                            <a>No:  { survey.no }</a>
                        </div>
                    </div> 
                );
            });
        } catch(e) {
            return;
        }
        
    }

    render() {
        
        return (
            <div>
                { this.renderSurveys() }
            </div>
            
        );
    }
}

function mapStateToProps(state) {
    return { surveys: state.surveys };
}

export default connect(mapStateToProps, { fetchSurveys })(SurveyList);