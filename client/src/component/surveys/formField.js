export default [
    { name: "title", label: "Survey Title", placeholder: "Enter your survey title", error: "You must provide a survey title" },
    { name: "subject", label: "Survey Subject", placeholder: "Enter your survey subject", error: "You must provide a survey subject" },
    { name: "body", label: "Survey Body", placeholder: "Enter your suvey message", error: "You must provide a survey body" },
    { name: "recipients", label: "Survey Recipient List", placeholder: "Enter recipient emails. E.g example@email.com, example2@email.com, etc..", error: "You must provide recipients email addresses" }
];