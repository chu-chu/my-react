import React from "react";
import { connect } from "react-redux";
import _ from "lodash";

import FIELDS from "./formField";
import * as actions from "../../actions/"
import { withRouter } from "react-router-dom";

const SurveyReview = ({ onCancel, formValues, submitSurvey, history }) => {

    const reviewField =  _.map(FIELDS, ({name, label}) => {
        return (
            <div key={name}>
                <label>{ label }</label>
                <div>
                    { formValues[name] }
                </div>  
            </div>
        );
    });

    return (
        <div>
            <h5>Please confirm your survey entries</h5>
            <div>
                <div>
                    { reviewField }
                </div>
            </div>
            <button 
                className="yellow darken-3 btn-flat"
                onClick={ onCancel }
            >
                Back
            </button>
            <button onClick={ () => submitSurvey(formValues, history)} className="green white-text waves-effect waves-light btn right">
                Send Survey
                <i className="material-icons right">email</i>
            </button>
        </div>
    );
}

function mapStateToProps(state) {
    return { formValues: state.form.surveyForm.values };
}

export default connect(mapStateToProps, actions)(withRouter(SurveyReview));