import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Paystack from "./Paystack";


class Header extends Component {
    renderContent() {
        switch(this.props.auth) {
            case null : 
                return "Loading data";
            case false :
                return (
                    <li><a href="/auth/google">Signin with Google</a></li>
                );
            default:
                return [
                    <li key="1"><Paystack /></li>,
                    <li key="2" style={{margin: '0 5px'}}><span>Credit: { this.props.auth.credits }</span></li>,
                    <li key="3"><a href="/api/logout">Logout</a></li>
                ];
        }
    };

    render() {
        return (
            <nav className="nav-extended">
            
                <div className="nav-wrapper">
                    <Link 
                    to={this.props.auth ? "/surveys" : "/"} 
                    className="brand-logo"
                    >
                        Emaily
                    </Link>
                    <a href="#" data-activates="mobile-demo" className="button-collapse"><i className="material-icons">menu</i></a>
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        { this.renderContent() }
                    </ul>
                    <ul className="side-nav" id="mobile-demo">
                        { this.renderContent() }
                    </ul>
                </div>
            </nav>
        )
    }
}


function mapStateToProps({ auth }) {
    return { auth }
}

export default connect(mapStateToProps)(Header);