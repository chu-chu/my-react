import React, { Component } from "react";
import PaystackButton from "react-paystack";
import { connect } from "react-redux";
import * as actions from "../actions";


class Paystack extends Component {
    state = {
        key: process.env.REACT_APP_PAYSTACK_KEY, // PAYSTACK PUBLIC KEY
        email: "chukwumdimma4life@gmail.com",
        amount: 1000000  
    }

    callback = async (response) => {
        // console.log(response); // card charged successfully, get reference here
        this.props.verifyPayment(response);
    }

    close = () => {
        console.log("Payment closed");
    }

    getReference = () => {
        //you can put any unique reference implementation code here
        
        const text = new Date().toDateString.toString;
        return text;
      
    }
    
    render() {
      
        
        return (
           
            <PaystackButton 
                text="BUY 100 CREDITS"
                class="waves-effect waves-light btn"
                callback={this.callback}
                close={this.close}
                reference={this.getReference()}
                email={this.state.email}
                amount={this.state.amount}
                paystackkey={this.state.key}
            />
         
        )
    }
}

export default connect(null,actions)(Paystack)