const { paystackSecretKey } = require("./../config/key")
const axios = require("axios");
const requireLogin = require("./../middlewares/requireLogin");


module.exports = (app) => {
    app.post("/api/paystack", requireLogin, async (req, res) => {
        try {
            const body = await axios({
                method: "get",
                url: "https://api.paystack.co/transaction/verify/"+ req.body.reference,
                headers: { 'Authorization': 'Bearer '+ paystackSecretKey }
            });
            if (!body.status) {
                throw new Error("Unexpected error")
            }
            let result = body.data;
            if (result.status === "failed") {
                throw new Error(result.gateway_response)
            }

            req.user.credits += 5;
            const user = await req.user.save();

            res.send(user);
    
        } catch(e) {
            res.status(401).send(e.message);
        }
        
    });
}