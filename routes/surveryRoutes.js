const _ = require("lodash")
const Path = require("path-parser");
const { URL } = require("url");
const requireLogin = require("./../middlewares/requireLogin");
const requireCredits = require("./../middlewares/requireCredits");
const { Survey } = require("./../models/Survey");
const Mailer = require("./../services/Mailer");
const surverysTemplate = require("./../services/emailTemplates/surveyTemplate");


module.exports = app => {
    app.get("/api/surveys", requireLogin, async (req, res) => {
        const surveys = await Survey.find({ _user: req.user.id })
            .select({ recipients: false });
        res.send(surveys);
    });

    app.get("/api/surveys/:surveyId/:choice", (req, res) => {
        res.send("Thanks for voting");
    });

    app.post("/api/surveys", requireLogin, requireCredits, async (req, res) => {
        try {
            const body = _.pick(req.body, ["title", "subject", "body", "recipients"]);
            body.recipients = body.recipients.split(",").map( email => { 
                return {email: email.trim()}
            });
            body._user = req.user.id;
            body.dateSent = Date.now();
            // Save surevey
            const sendMessage = await new Survey(body).save();
            // Send mail
            const mailer = new Mailer(sendMessage, surverysTemplate(sendMessage));
            await mailer.send();

            req.user.credits -= 1;
            const user = await req.user.save();

            res.status(200).send(user);
        } catch(e) {
            res.status(422).send(e);
        }
    });

    app.post("/api/surveys/webhook", (req, res) => {
        const p = new Path("/api/surveys/:surveyId/:choice");
        _.chain(req.body)
            .map(({email, url}) => {
            let match = p.test(new URL(url).pathname);
            if (match) {
                return { 
                    email,
                    surveyId: match.surveyId,
                    choice: match.choice
                };
            }
        })
        .compact()
        .uniqBy("email", "surveyId")
        .each(({ surveyId, email, choice }) => {
            Survey.updateOne({
                _id: surveyId,
                recipients: {
                    $elemMatch: { email, responded: false }
                }
            }, {
                $inc: { [choice] : 1 },
                $set: { "recipients.$.responded": true },
                lastResponded: Date.now()
            }).exec();
        })
        .value();

        res.send({});
    });

};