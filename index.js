const express = require("express");
const mogoose = require("mongoose");
var bodyParser = require('body-parser')
const cookieParser = require("cookie-parser");
const session = require("express-session");
const MongoStore = require("connect-mongodb-session")(session);
const passport = require("passport");
const path = require("path");

const {mongoURI, cookieKey} = require("./config/key");

require("./services/passport");

mogoose.connect(mongoURI, (err) => {
    if (err) {
        console.log("Failded to connect to database");
    } else {
        console.log("db connection was successfull");
    }
})

const app = express();

app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: cookieKey,
    cookie: { maxAge: 1000 * 60 * 60 * 24 * 7 }, // 1 week
    store: new MongoStore({uri: mongoURI, autoReconnect: true, collection: "mySessions"})
}));
app.use(passport.initialize());
app.use(passport.session());

require("./routes/authRoutes")(app);
require("./routes/paymentRoutes")(app);
require("./routes/surveryRoutes")(app);

if (process.env.NODE_ENV === "production") {
    // Express will server up production asset
    // Like our main.js file, or main.css file!
    app.use(express.static("client/build"));

    // Express will serve up the index.html files
    // if it doesn't recognize the route
    app.get("*", (req, res) => {
        res.sendFile(path.resolve(__dirname, "client", "build", "index.html"))
    });
}

const port = process.env.PORT || 3001;
app.listen(port,() => {
    console.info(`server started on port ${port}`);
});
