const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const keys = require("./../config/key");
const { Users } = require("./../models/User");

passport.serializeUser((user, next) => {
    next(null, user.id);
});

passport.deserializeUser(async (id, next) => {
    let user = await Users.findById(id);
    next(null, user);
});

passport.use(new GoogleStrategy({
    clientID: keys.googleClientID,
    clientSecret: keys.googleClientSecret,
    callbackURL: "/auth/google/callback",
    proxy: true
}, async (accessToken, refreshToken, profile, next) => {
    try {
        let user = await Users.findOne({googleId: profile.id});
        if (user) {
            // User already exist just loggin
            next(null,  user);
        } else {
            user = await new Users({
                googleId: profile.id
            }).save();
            next(null, user);
        }

    } catch(e) {
        console.log(e);
    }
    // check if id exist
    
    // console.log("Access Token",accessToken);
    // console.log("Refresh Token",refreshToken);
    // console.log("Profile: ",profile);
}));
