const mongoose = require("mongoose");
const  Schema  = mongoose.Schema;

var UserSchema = new Schema({
    googleId: String,
    credits: { 
        type: Number,
        default: 0
    }
});

let Users = mongoose.model("Users", UserSchema);

module.exports = { Users }